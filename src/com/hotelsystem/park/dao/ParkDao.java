package com.hotelsystem.park.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.RowProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.hotelsystem.common.entity.Park;
import com.hotelsystem.common.utils.DBUtil;


public class ParkDao
{
private QueryRunner queryRunner = new QueryRunner( DBUtil.getDataSource() );
	
	

	
	public int updateCar(Park park) throws SQLException{
		String sql = "UPDATE " +
				"			PARK" +
				"	 SET CARID = ?" +
				"	WHERE " +
				"		 CARPOS = ?";
		return queryRunner.update(
					sql, park.getCarId(),park.getCarPos());
	}
	
	public Park findById( String carPos ){
		try {
			String sql = "SELECT * FROM PARK WHERE CARPOS = ?";
			
			BeanProcessor bean = new GenerousBeanProcessor();
			RowProcessor processor = new BasicRowProcessor(bean);
			return queryRunner.query(sql, new BeanHandler<Park>(Park.class,processor), carPos);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Park> findAll(){
		
		try {
			String sql = "SELECT * FROM PARK";
			BeanProcessor bean = new GenerousBeanProcessor();
			RowProcessor processor = new BasicRowProcessor(bean);
			return queryRunner.query(sql, new BeanListHandler<Park>(Park.class,processor));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
