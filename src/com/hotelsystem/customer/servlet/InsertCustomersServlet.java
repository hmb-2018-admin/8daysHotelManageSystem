package com.hotelsystem.customer.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotelsystem.common.entity.Customer;
import com.hotelsystem.common.entity.Park;
import com.hotelsystem.common.entity.Room;
import com.hotelsystem.customer.service.CustomerService;

@WebServlet("/hotel/customer/insert")
public class InsertCustomersServlet extends HttpServlet{
	private CustomerService customerService=new CustomerService();

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Customer cus=new Customer();
		Room room=new Room();
		Park park=new Park();
		
		room.setRoomNum(Integer.parseInt(req.getParameter("roomNum")));
		room.setRoomState(true);
		park.setCarId(req.getParameter("customerCarId"));
		park.setCarPos(req.getParameter("carPos"));
		
		String mark=req.getParameter("vipMark");
		
		boolean flag;
		if(mark.equals("1") || mark.equals("true") || mark.equals("是")) flag=true;
		else flag=false;
		
		cus.setCustomerId(req.getParameter("customerId"));
		cus.setCarPos(req.getParameter("carPos"));
		cus.setRoomNum(Integer.parseInt(req.getParameter("roomNum")));
		cus.setCustomerName(req.getParameter("customerName"));
		cus.setCustomerSex(req.getParameter("customerSex"));
		cus.setVipMark(flag);
		cus.setCustomerPhone(req.getParameter("customerPhone"));
		cus.setCustomerCarId(req.getParameter("customerCarId"));
		
		customerService.insertCustomers(cus);
		customerService.updateRoomStateByCus(room);
		customerService.updateCarIdByCus(park);
		resp.sendRedirect("/hotelmanagesystem/hotel/index");
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("/WEB-INF/views/customer/insertcustomers.jsp").forward(req, resp);
	}
	
	
}
