package com.hotelsystem.customer.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotelsystem.common.entity.Park;
import com.hotelsystem.common.entity.Room;
import com.hotelsystem.customer.service.CustomerService;

@WebServlet("/hotel/customer/delete")
public class DeleteCustomersServlet extends HttpServlet{

	private CustomerService customerService=new CustomerService();
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String roomnum=req.getParameter("roomNum");
		
		Room room=new Room();
		Park park=new Park();
		
		room.setRoomNum(Integer.parseInt(req.getParameter("roomNum")));
		room.setRoomState(false);
		park.setCarId("");
		park.setCarPos(req.getParameter("carPos"));
		
		try {
			if(roomnum!=null){
				customerService.deleteCustomers(Integer.parseInt(roomnum));
				customerService.updateRoomStateByCus(room);
				customerService.updateCarIdByCus(park);
				resp.sendRedirect("/hotelmanagesystem/hotel/index");
			}
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
