package com.hotelsystem.customer.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotelsystem.common.entity.Customer;
import com.hotelsystem.common.entity.EmployeeProperties;
import com.hotelsystem.common.entity.Park;
import com.hotelsystem.common.entity.Room;
import com.hotelsystem.customer.service.CustomerService;
import com.hotelsystem.employee.service.EmployeeService;
import com.hotelsystem.park.service.ParkService;
import com.hotelsystem.room.service.RoomService;



@WebServlet("/hotel/customer/getbyroom")
public class GetCustomersByRoomServlet extends HttpServlet{
	private CustomerService customerService=new CustomerService();
	private RoomService roomService=new RoomService();
	private ParkService parkService=new ParkService();
	private EmployeeService employeeService=new EmployeeService();

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setCharacterEncoding("UTF-8");
		resp.setContentType("text/html;charset=UTF-8");
		
		List<Room> rooms=roomService.getEmptyRooms();
		List<Park> parks=parkService.getCars();
		List<EmployeeProperties> employees=employeeService.allQuery();
		
		req.setAttribute("park", parks);
		req.setAttribute("employeeProperties", employees);
		req.setAttribute("rooms", rooms);
		
		String str=req.getParameter("roomnum");
		if(str==null){
			str="";
		}
		if(str!=""){
			int roomnum=Integer.parseInt(req.getParameter("roomnum"));
			List<Customer> customers=customerService.getCustomersByRoomNum(roomnum);
			req.setAttribute("customers", customers);
			req.getRequestDispatcher("/WEB-INF/views/main/index.jsp").forward(req, resp);
		}
	}
//
//	@Override
//	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//		req.getRequestDispatcher("/WEB-INF/views/customer/getcustomersbyroom.jsp").forward(req, resp);
//	}
//	
//	
}
