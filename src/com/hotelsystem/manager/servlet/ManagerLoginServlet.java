package com.hotelsystem.manager.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotelsystem.common.entity.Manager;
import com.hotelsystem.manager.dao.ManagerLoginDao;

@WebServlet("/hotel/manager/login")
public class ManagerLoginServlet extends HttpServlet{
	
	private ManagerLoginDao mld=new ManagerLoginDao();


	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String username=req.getParameter("userName");
		String password=req.getParameter("passWord");
		
		Manager man=new Manager();
		
		
		man=mld.findOne(username, password);
		
		if(man==null){
			resp.sendRedirect("/hotelmanagesystem/assert/login/login.html");
		}
		else{
			resp.sendRedirect("/hotelmanagesystem/hotel/index");
		}
	}
	
	
}
