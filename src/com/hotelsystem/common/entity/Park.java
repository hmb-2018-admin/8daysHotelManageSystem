package com.hotelsystem.common.entity;

public class Park
{
	private String carId;
	private String carPos;
	public String getCarId() {
		return carId;
	}
	public void setCarId(String carId) {
		this.carId = carId;
	}
	public String getCarPos() {
		return carPos;
	}
	public void setCarPos(String carPos) {
		this.carPos = carPos;
	}

}
