package com.hotelsystem.common.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotelsystem.common.entity.Customer;
import com.hotelsystem.common.entity.EmployeeProperties;
import com.hotelsystem.common.entity.Park;
import com.hotelsystem.common.entity.Room;
import com.hotelsystem.customer.service.CustomerService;
import com.hotelsystem.employee.service.EmployeeService;
import com.hotelsystem.park.service.ParkService;
import com.hotelsystem.room.service.RoomService;

@WebServlet("/hotel/index")
public class CommonGetAllServlet extends HttpServlet{
	private RoomService roomService=new RoomService();
	private CustomerService customerService=new CustomerService();
	private ParkService parkService=new ParkService();
	private EmployeeService employeeService=new EmployeeService();
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Room> rooms=roomService.getRooms();
		List<Customer> customers=customerService.getCustomers();
		List<Park> parks=parkService.getCars();
		List<EmployeeProperties> employees=employeeService.allQuery();
		
		req.setAttribute("rooms", rooms);
		req.setAttribute("customers", customers);
		req.setAttribute("park", parks);
		req.setAttribute("employeeProperties", employees);
		
		req.getRequestDispatcher("/WEB-INF/views/main/index.jsp").forward(req, resp);
	}
	
	
}
