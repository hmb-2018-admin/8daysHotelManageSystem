package com.hotelsystem.employee.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotelsystem.common.entity.EmployeeProperties;
import com.hotelsystem.employee.service.EmployeeService;

@WebServlet("/hotel/employee")
public class AllEmployeeServlet extends HttpServlet{
    private EmployeeService allemployeeService=new EmployeeService();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<EmployeeProperties> employeeProperties=allemployeeService.allQuery();
		req.setAttribute("employeeProperties", employeeProperties);
		req.getRequestDispatcher("/WEB-INF/views/main/index.jsp").forward(req, resp);
	}
   
}
