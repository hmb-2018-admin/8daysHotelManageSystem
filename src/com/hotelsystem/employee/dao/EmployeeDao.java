package com.hotelsystem.employee.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.RowProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.hotelsystem.common.entity.EmployeeProperties;
import com.hotelsystem.common.utils.DBUtil;


public class EmployeeDao {
   private QueryRunner queryRunner=new QueryRunner(DBUtil.getDataSource());
   
   public List<EmployeeProperties> AllQuery(){
	   String sql="select * from employee";
	   BeanProcessor bean = new GenerousBeanProcessor();
		// 将GenerousBeanProcessor对象传递给BasicRowProcessor
		RowProcessor processor = new BasicRowProcessor(bean);
	   try {
		return queryRunner.query(sql, new BeanListHandler<EmployeeProperties>(EmployeeProperties.class,processor));
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return null;
   }
   public int DeleteEmployee(String empWorkid) throws SQLException{
	   String sql="DELETE FROM EMPLOYEE WHERE EMPWORKID=?";
	   
		return queryRunner.update(sql,empWorkid);
	
   }
   public int InsertEmployee(EmployeeProperties employeeProperties) throws SQLException
   {
 	  String sql="INSERT INTO EMPLOYEE"+
   "(EMPWORKID,EMPNAME,EMPSEX,EMPID,EMPPOS,EMPSER) VALUES(?,?,?,?,?,?)";
 	  return queryRunner.update(sql,employeeProperties.getEmpWorkid(),
 			  employeeProperties.getEmpName(),
 			 employeeProperties.getEmpSex(),employeeProperties.getEmpId(),
 			employeeProperties.getEmpPos(),employeeProperties.getEmpSer());
   }
   public int UpdateEmployee(EmployeeProperties employeeProperties) throws SQLException{
	  String sql="UPDATE EMPLOYEE SET EMPNAME=?,EMPSEX=?,EMPID=?,"+
        "EMPPOS=?,EMPSER=? WHERE EMPWORKID=?";
	  
	  return queryRunner.update(sql,employeeProperties.getEmpName(),
 			 employeeProperties.getEmpSex(),employeeProperties.getEmpId(),
 			employeeProperties.getEmpPos(),employeeProperties.getEmpSer(),employeeProperties.getEmpWorkid());
	   
   }
   public List<EmployeeProperties> SingalQuery(String empworkid) throws SQLException{
	   String sql="SELECT * FROM EMPLOYEE WHERE EMPWORKID=?";
	   return queryRunner.query(sql,new BeanListHandler<EmployeeProperties>(EmployeeProperties.class),empworkid);
	   
   }
}
