package com.hotelsystem.room.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.RowProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.hotelsystem.common.entity.Room;
import com.hotelsystem.common.utils.DBUtil;



/**
*@author:lyb 
*@version:
*@date:2018年6月29日上午10:30:50
*
*/
public class RoomDao
{
private QueryRunner queryRunner = new QueryRunner( DBUtil.getDataSource() );
	
	public int insert(Room room) throws SQLException{
		
		String sql = "INSERT INTO ROOM" +
				"(roomNum,roomState,roomMold,roomCost) VALUES(?,?,?,?)";
		return queryRunner.update(
				sql, 
				room.getRoomNum(),room.isRoomState(),room.getRoomMold(),room.getRoomCost());
	}
	
	public int delete( int roomNum ) throws SQLException{
		String sql = "DELETE FROM ROOM WHERE roomNum = ?";
		return queryRunner.update( sql, roomNum);
	}

	
	public int update(Room room) throws SQLException{
		String sql = "UPDATE " +
				"			room" +
				"	 SET roomState = ?,roomMold = ?,roomCost = ?" +
				"	WHERE " +
				"		roomNum = ?";
		return queryRunner.update(
					sql, 
					room.isRoomState(),room.getRoomMold(),room.getRoomCost(),room.getRoomNum());
	}
	
	public Room findById( int roomNum ){
		try {
			String sql = "SELECT * FROM Room WHERE roomNum = ?";
			
			BeanProcessor bean = new GenerousBeanProcessor();
			RowProcessor processor = new BasicRowProcessor(bean);
			return queryRunner.query(sql, new BeanHandler<Room>(Room.class,processor), roomNum);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Room> findAll(){
		
		try {
			String sql = "SELECT * FROM Room";
			BeanProcessor bean = new GenerousBeanProcessor();
			RowProcessor processor = new BasicRowProcessor(bean);
			return queryRunner.query(sql, new BeanListHandler<Room>(Room.class,processor));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Room> findEmpty(){
		
		try {
			String sql = "SELECT * FROM Room where roomState = '0'";
			BeanProcessor bean = new GenerousBeanProcessor();
			RowProcessor processor = new BasicRowProcessor(bean);
			return queryRunner.query(sql, new BeanListHandler<Room>(Room.class,processor));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
