package com.hotelsystem.room.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotelsystem.common.entity.EmployeeProperties;
import com.hotelsystem.common.entity.Room;
import com.hotelsystem.room.dao.RoomDao;




@WebServlet("/hotel/room/update")
public class RoomUpdateServlet extends HttpServlet {
	
	private RoomDao roomDao = new RoomDao();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String roomNum=req.getParameter("roomNum");
		Room room=roomDao.findById(Integer.parseInt(roomNum));	
			req.setAttribute("room", room);
			req.getRequestDispatcher("/WEB-INF/views/room/RoomUpdate.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		req.setCharacterEncoding("UTF-8");
		resp.setCharacterEncoding("UTF-8");
		
		String roomNum = req.getParameter("roomNum");
		String roomState = req.getParameter("roomState");
		String roomMold = req.getParameter("roomMold");
		String roomCost=req.getParameter("roomCost");
		
		boolean flag;
		if(roomState.equals("0") || roomState.equals("false") || roomState.equals("空"))
			flag=false;
		else flag=true;
		
		Room room = new Room();
		room.setRoomNum( Integer.parseInt(roomNum) );
		room.setRoomState(flag);
		room.setRoomMold(roomMold);
		room.setRoomCost(Integer.parseInt(roomCost));

		
		
		try {
			roomDao.update(room);
			
			resp.sendRedirect("/hotelmanagesystem/hotel/index");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
}
