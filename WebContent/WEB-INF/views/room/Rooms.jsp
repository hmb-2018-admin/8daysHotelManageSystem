<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>


</head>
<body>
	<div>
		<table>
		<c:forEach items="${ rooms }"  var="R">
			<tr>
				<td>
					${R.roomNum }
				</td>
				<td>
					${R.roomState }
				</td>
				<td>
					${R.roomMold }
				</td>
				<td>
					<a href="/hotel/update?roomNum=${R.roomNum }">修改</a>
				</td>
			</tr>
		</c:forEach>
	</table>
	</div>
</body>
</html>