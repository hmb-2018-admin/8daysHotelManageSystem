<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<div>
	<form action="/hotelmanagesystem/hotel/customer/getbyroom" method="post">
		<input type="text" name="roomnum"/>
		<input type="submit" value="submit"/>
		<br/>
		<label>房间入住人信息...</label>
		<br/>
		<c:forEach items="${customers }" var="a">
			<tr>
				<td>
					${a.customerId }
				</td>
				<td>
					${a.carPos }
				</td>
				<td>
					${a.roomNum }
				</td>
				<td>
					${a.customerName }
				</td>
				<td>
					${a.customerSex }
				</td>
				<td>
					${a.vipMark }
				</td>
				<td>
					${a.customerPhone }
				</td>	
				<td>
					${a.customerCarId }
				</td>				
			</tr>
			<br/>
		</c:forEach>
		<a href="/hotelmanagesystem/hotel/index">点击此处返回</a>	
	</form>
</div>
</body>
</html>