<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">

</style>
</head>
<body>
   <table>
      <c:forEach items="${employeeProperties}" var="E">
          <tr>
             <td>
                ${E.empWorkid }
             </td>
             <td>
                ${E.empName }
             </td>
             <td>
                ${E.empSex }
             </td>
             <td>
                ${E.empId }
             </td>
             <td>
                ${E.empPos }
             </td>
             <td>
                ${E.empSer }
             </td>
             <td>
               <a href="/hotelmanagesystem/hotel/employee/delete?empWorkid=${E.empWorkid } ">删除</a>
             </td>
             <td>
               <a href="/hotelmanagesystem/hotel/employee/update?empWorkid=${E.empWorkid }">修改</a>
             </td>
          </tr>
      </c:forEach>
   </table>
   <div>
     <a href="/hotelmanagesystem/hotel/employee/insert">添加</a>
   </div>

</body>
</html>