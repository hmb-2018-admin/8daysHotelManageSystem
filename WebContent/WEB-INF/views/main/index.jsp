<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript" src="http://www.francescomalagrino.com/BootstrapPageGenerator/3/js/jquery-2.0.0.min.js"></script>
<script type="text/javascript" src="http://www.francescomalagrino.com/BootstrapPageGenerator/3/js/jquery-ui"></script>
<link href="http://www.francescomalagrino.com/BootstrapPageGenerator/3/css/bootstrap-combined.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" type="text/css" href="/assert/css/bootstrap.css">
<script type="text/javascript" src="http://www.francescomalagrino.com/BootstrapPageGenerator/3/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="tabbable" id="tabs-149066">
				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#panel-1" data-toggle="tab">房间管理</a>
					</li>
					<li>
						<a href="#panel-2" data-toggle="tab">顾客管理</a>
					</li>
					<li>
						<a href="#panel-3" data-toggle="tab">车库管理</a>
					</li>
					<li>
						<a href="#panel-4" data-toggle="tab">员工管理</a>
					</li>
				</ul>
				<a href="/hotelmanagesystem/assert/login/login.html">注销</a>
				<div class="tab-content">
					
					<div class="tab-pane active" id="panel-1">
						<p onclick="/hotelmanagesystem/hotel/rooms">
							<form >
								房间管理.
								&nbsp;
								<a href="/hotelmanagesystem/hotel/emptyrooms">查询空房间</a>
								&nbsp;
								<a href="/hotelmanagesystem/hotel/index">查询所有房间</a>
							</form>
							
							<table class="table">
										<thead>
											<tr>
												<th>
													房间号
												</th>
												<th>
													房间状态
												</th>
												<th>
													房间类型
												</th>
												<th>
													房间价钱
												</th>
											</tr>
										</thead>
										<tbody>
										<c:forEach items="${ rooms }"  var="R">
												<tr class="success">
													<td>
														${R.roomNum }
													</td>
													<td>
														${R.roomState }
													</td>
													<td>
														${R.roomMold }
													</td>
													<td>
														${R.roomCost }
													</td>
													<td>
														<a href="/hotelmanagesystem/hotel/room/update?roomNum=${R.roomNum }">修改</a>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
						</p>
					</div>
					<div class="tab-pane" id="panel-2">
						<p onclick="/hotelmanagesystem/hotel/customer/getall">
							<form action="/hotelmanagesystem/hotel/customer/getbyroom" method="POST">
								顾客管理.
								&nbsp;
							  	<input type="text" name="roomnum">
							  	<input type="submit" value="查询">
							  	&nbsp;
							  	<a href="/hotelmanagesystem/hotel/index">查询全部顾客</a>
							  	&nbsp;
								<a href="/hotelmanagesystem/hotel/customer/insert">添加入住人信息</a>	
							</form>
										
							<table class="table">
										<thead>
											<tr>
												<th>
													房间号
												</th>
												<th>
													姓名
												</th>
												<th>
													性别
												</th>
												<th>
													身份证号
												</th>
												<th>
													车位号
												</th>
												<th>
													联系电话
												</th>
												<th>
													会员
												</th>
												<th>
													车牌号
												</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${ customers }"  var="C">
												<tr class="success">
													<td>
														${C.roomNum }
													</td>
													<td>
														${C.customerName }
													</td>
													<td>
														${C.customerSex }
													</td>
													<td>
														${C.customerId }
													</td>
													<td>
														${C.carPos }
													</td>
													<td>
														${C.customerPhone }
													</td>
													<td>
														${C.vipMark }
													</td>
													<td>
														${C.customerCarId }
													</td>
													<td>
														<a href="/hotelmanagesystem/hotel/customer/update?customerId=${C.customerId }">修改</a>
													</td>
													<td>
														<a href="/hotelmanagesystem/hotel/customer/delete?roomNum=${C.roomNum }">删除</a>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
						</p>
					</div>

					<div class="tab-pane" id="panel-3">
						<p onclick="/hotelmanagesystem/hotel/park">
							<form >
								车库管理.
							</form>
							
							<table class="table">
										<thead>
											<tr>
												<th>
													车牌号
												</th>
												<th>
													车位
												</th>
												
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${ park }"  var="P">
												<tr class="success">
													<td>
														${P.carId }
													</td>
													<td>
														${P.carPos }
													</td>
									
												</tr>
											</c:forEach>
										</tbody>
									</table>
						     </p>
					</div>

					<div class="tab-pane" id="panel-4">
						<p onclick="/hotelmanagesystem/hotel/employee">
							
							<form action="/hotelmanagesystem/hotel/singlequery" method="POST">
								员工管理.
								&nbsp;
							  	<input type="text" name="empWorkid">
							  	<input type="submit" value="查询">
							  	&nbsp;
							  	<a href="/hotelmanagesystem/hotel/index">查询所有员工</a>
								<a href="/hotelmanagesystem/hotel/employee/insert">添加</a>
							</form>
							
							
							<table class="table">
										<thead>
											<tr>
												<th>
													工作证号
												</th>
												<th>
													员工姓名
												</th>
												<th>
													性别
												</th>
												<th>
													身份证号
												</th>
												<th>
													员工职位
												</th>
												<th>
													员工薪水
												</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${employeeProperties}" var="E">
									          <tr class="success">
									             <td>
									                ${E.empWorkid }
									             </td>
									             <td>
									                ${E.empName }
									             </td>
									             <td>
									                ${E.empSex }
									             </td>
									             <td>
									                ${E.empId }
									             </td>
									             <td>
									                ${E.empPos }
									             </td>
									             <td>
									                ${E.empSer }
									             </td>
									             <td>
									               <a href="/hotelmanagesystem/hotel/employee/delete?empWorkid=${E.empWorkid }">删除</a>
									             </td>
									             <td>
									               <a href="/hotelmanagesystem/hotel/employee/update?empWorkid=${E.empWorkid }">修改</a>
									             </td>
									          </tr>
									      </c:forEach>
	          						 </tbody>
      							</table>
							</p>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>